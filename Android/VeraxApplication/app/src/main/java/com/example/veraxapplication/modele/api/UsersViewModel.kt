package com.example.veraxapplication.modele.api

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.veraxapplication.modele.articles.Article
import com.example.veraxapplication.modele.user.User
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClientUser {

    private const val BASE_URL = "https://codefirst.iut.uca.fr/containers/Verax-verax-api"

    private val logging = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    private val httpClient = OkHttpClient.Builder().apply {
        addInterceptor(logging)
    }.build()


    val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

//    interface UserApiService {
//        @GET("users")
//        suspend fun getUsers() : List<User>
//    }
}

class UsersViewModel : ViewModel() {
    private val _users = MutableLiveData<List<User>>()
    val users: LiveData<List<User>> = _users

    private val service = ArticleApiClient.retrofit.create(IUserService::class.java)

    init {
        loadUsers()
    }

    fun loadUsers() {
        viewModelScope.launch {
            try {
                val usersDto = service.getUsers() // Pas besoin d'appeler .execute()
                // Convertissez les DTO en modèles de données si nécessaire
                _users.value = usersDto.map { it.toModel() }
            } catch (e: Exception) {
                Log.e("UsersViewModel", "Erreur lors du chargement des users", e)
            }
        }
    }
}