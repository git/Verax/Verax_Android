
import com.example.veraxapplication.modele.IArticlesDataManager
import com.example.veraxapplication.modele.articles.Article
import com.example.veraxapplication.modele.articles.contenus.Contenu
import com.example.veraxapplication.modele.articles.contenus.ContenuMedia
import com.example.veraxapplication.modele.articles.contenus.ContenuParagraphe
import com.example.veraxapplication.modele.user.User


class StubUsers() : IUsersDataManager {
    private var lUsers: MutableList<User>? = null

    init {
        chargerUsers()
    }


    private fun chargerUsers() {
        lUsers = java.util.ArrayList<User>()
        val user1 = User (
            "NoaSil",
            "1234",
            "",
            "Sillard",
            "Noa",
            "Admin"
        )
        lUsers!!.add(user1)
        val user2 = User (
            "Sha",
            "1234",
            "",
            "Cascarra",
            "Shana",
            "Admin"
        )
        lUsers!!.add(user2)
        val user3 = User (
            "TonyF",
            "1234",
            "tony@gmail.com",
            "Fages",
            "Tony",
            "Admin"
        )
        lUsers!!.add(user3)
        val user4 = User (
            "JeanSwaggLaPuissance63",
            "1234",
            "jean.lapuissance@gmail.com",
            "Marcillac",
            "Jean",
            "Admin"
        )
        lUsers!!.add(user4)
    }

    override val allUsers: List<User>?
        get() = lUsers

    override fun getUser(pseudo : String): User? {
        println("Passage dans getUser avec comme pseudo : $pseudo")
        lUsers?.let {
            println("Nombre d'utilisateurs disponibles : ${it.size}")

            val userARenvoyer: User? = it.find { user -> user.pseudo == pseudo }

            return userARenvoyer
        }
        return null
    }

    override fun getUsers(): List<User>
    {
        return lUsers?.takeIf { it.isNotEmpty() }?.take(lUsers!!.size) ?: emptyList()
    }

}