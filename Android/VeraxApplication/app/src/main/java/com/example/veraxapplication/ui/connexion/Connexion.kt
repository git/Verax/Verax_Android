package com.example.veraxapplication.ui.connexion

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.veraxapplication.modele.user.User

@Composable
fun AfficherForm(users : List<User>) {
    var pseudo = "DEFAULT"
    var mdp = "DEFAULT"
    Column {
        TextField(value = "Pseudo", onValueChange = { value -> pseudo = value }, modifier = Modifier.padding(5.dp))
        TextField(value = "Mot de passe", onValueChange = { value -> mdp = value }, modifier = Modifier.padding(5.dp))
        for (u in users) {
            Text(text = u.pseudo)
        }
    }
}