package com.example.veraxapplication.modele.api

import com.example.veraxapplication.modele.user.User
import retrofit2.Call
import retrofit2.http.GET

interface IUserService {
    @GET("users")
    suspend fun getUsers(): List<UserDTO>

}