package com.example.veraxapplication.modele.api

import com.example.veraxapplication.modele.user.User
import com.google.gson.annotations.SerializedName

data class UserDTO (

    @SerializedName("pseudo")
    val pseudo: String,
    @SerializedName("mdp")
    val mdp: String,
    @SerializedName("mail")
    val mail: String,
    @SerializedName("nom")
    val nom: String,
    @SerializedName("prenom")
    val prenom: String,
    @SerializedName("role")
    val role: String,

    ) {
    fun toModel(): User {
        return User(
            pseudo,
            mdp,
            mail,
            nom,
            prenom,
            role,
            )
    }
}